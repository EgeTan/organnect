import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from '../views/LoginView.vue'
import GeneralView from '../views/GeneralView.vue'
import KidneyFunctionalityView from '../views/KidneyFunctionalityView.vue'
import OfferedOrganView from '../views/OfferedOrganView.vue'
import OfferAView from '../views/OfferAView.vue'
import OfferBView from '../views/OfferBView.vue'
import DoctorGeneralView from "../views/DoctorGeneralView.vue"
import DoctorHealthDataView from "../views/DoctorHealthDataView.vue"
import DoctorStatusPredictionView from "../views/DoctorStatusPredictionView.vue"

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/general',
    name: 'general',
    component: GeneralView
  },
  {
    path: '/kidneyFunction',
    name: 'kidneyFunction',
    component: KidneyFunctionalityView
  },
  {
    path: '/offeredOrgan',
    name: 'offeredOrgan',
    component: OfferedOrganView
  },
  {
    path: '/offer/:id',
    component: OfferAView
  },
  {
    path: '/offerB',
    name: 'offerB',
    component: OfferBView
  },
  {
    path: '/doctorGeneral',
    name: 'doctorGeneral',
    component: DoctorGeneralView
  },
  {
    path: '/doctorHealthData/:id',
    name: 'doctorHealthData',
    component: DoctorHealthDataView
  },
  {
    path: '/doctorStatusPrediction',
    name: 'doctorStatusPrediction',
    component: DoctorStatusPredictionView
  },
]

const router = new VueRouter({
  routes
})

export default router
